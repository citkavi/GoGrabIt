package com.gograbit.gograbit.ui.signup.model

import com.google.gson.annotations.SerializedName


/**
 * Created by kavi on 21/02/18.
 */

data class SignUpRequest(
        @SerializedName("member_fname") var memberFname: String = "",
        @SerializedName("member_lname") var memberLname: String = "",
        @SerializedName("member_username") var memberUsername: String = "",
        @SerializedName("member_password") var memberPassword: String = "",
        @SerializedName("member_role_id") var memberRoleId: Int = 1,
        @SerializedName("password_confirm") var password_confirm: String = ""

/* @SerializedName("member_reset_hashcode") var memberResetHashcode: String = "",
 @SerializedName("member_reset_time") var memberResetTime: Int = 0,
 @SerializedName("member_lastattempt") var memberLastattempt: Int = 0,
 @SerializedName("member_lastsession") var memberLastsession: Int = 0,

 @SerializedName("member_address1") var memberAddress1: String = "",
 @SerializedName("member_address2") var memberAddress2: String = "",
 @SerializedName("member_city") var memberCity: String = "",
 @SerializedName("member_prov") var memberProv: Int = 0,
 @SerializedName("member_country") var memberCountry: Int = 0,
 @SerializedName("member_postal") var memberPostal: String = "",
 @SerializedName("member_status") var memberStatus: Int = 0,
 @SerializedName("created_by") var createdBy: Int = 0*/
)