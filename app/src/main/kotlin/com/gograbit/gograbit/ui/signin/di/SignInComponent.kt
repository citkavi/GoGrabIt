package com.gograbit.gograbit.ui.signin.di

import com.gograbit.gograbit.base.di.ActivityScope
import com.gograbit.gograbit.base.di.component.AppComponent
import com.gograbit.gograbit.ui.signin.SignInActivity
import dagger.Component

/**
 * Created by kavi on 21/02/18.
 */


@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(SignInModule::class))
interface SignInComponent {

    fun inject(signInActivity: SignInActivity)
}
