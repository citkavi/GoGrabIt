package com.gograbit.gograbit.ui.splash

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.gograbit.gograbit.R
import com.gograbit.gograbit.ui.signin.SignInActivity
import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        imageView.postDelayed(Runnable {

            val intent = Intent(this, SignInActivity::class.java)
// Pass data object in the bundle and populate details activity.
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, imageView as View, "profile")
            startActivity(intent, options.toBundle())
            finish()

        }, 3000)

    }
}
