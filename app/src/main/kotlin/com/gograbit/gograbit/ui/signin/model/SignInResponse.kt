package com.gograbit.gograbit.ui.signin.model
import com.google.gson.annotations.SerializedName


/**
 * Created by kavi on 21/02/18.
 */

data class SignInResponse(
		@SerializedName("status") var status: Boolean = false,
		@SerializedName("message") var message: String = ""
)