package com.gograbit.gograbit.ui.signin.presenter

import com.gograbit.gograbit.base.api.EndPoints
import com.gograbit.gograbit.base.di.mvp.BasePresenter
import com.gograbit.gograbit.ui.signup.model.SignUpRequest
import com.gograbit.gograbit.ui.signup.model.SignUpResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.regex.Pattern
import javax.inject.Inject

/**
 * Created by kavi on 21/02/18.
 */


class SignUpPresenter @Inject constructor(var api: EndPoints) : BasePresenter<SignUpView>() {


    fun registerUser(firstName: String, lastName: String, userName: String, password: String) {
        view?.showProgress()

        api.signUpUSer(SignUpRequest(firstName, lastName, userName, password, 1, password)).enqueue(object : Callback<SignUpResponse> {
            override fun onResponse(call: Call<SignUpResponse>, response: Response<SignUpResponse>) {
                view?.hideProgress()

                if (response.isSuccessful) {

                    view?.navigateToListActivity()
                } else
                    view?.onError()

            }

            override fun onFailure(call: Call<SignUpResponse>, t: Throwable) {
                view?.hideProgress()
                view?.onError()
            }
        })
    }

    fun validateFields(emailId: String, password: String, firstName: String, lastName: String): Boolean {
        if (isFirstNameNotEmpty(firstName) && isLastNameNotEmpty(lastName) && isEmailNotEmpty(emailId) && isValidEmail(emailId) && isPasswordNotEmpty(password))
            return true

        return false
    }


    fun isValidEmail(emailId: String): Boolean {
        if (isEmailNotEmpty(emailId) && isEmailValid(emailId))
            return true

        return false
    }

    fun isEmailNotEmpty(emailId: String): Boolean {
        if (emailId.length == 0) {
            view?.setEmailEmptyError()
            return false
        }
        return true
    }


    fun isFirstNameNotEmpty(firstname: String): Boolean {
        if (firstname.length == 0) {
            view?.setFirstnameEmptyError()
            return false
        }
        return true
    }


    fun isLastNameNotEmpty(lastName: String): Boolean {
        if (lastName.length == 0) {
            view?.setLastnameEmptyError()
            return false
        }
        return true
    }


    fun isEmailValid(emailId: String): Boolean {
        if (!Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(emailId).matches()) {
            view?.setEmailInvalidError()
            return false
        }
        return true
    }

    fun isPasswordNotEmpty(password: String): Boolean {
        if (password.length == 0) {
            view?.setPasswordEmptyError()
            return false
        }
        return true
    }


}
