package com.gograbit.gograbit.ui.signup.di

import com.gograbit.gograbit.base.api.EndPoints
import com.gograbit.gograbit.base.di.ActivityScope
import com.gograbit.gograbit.ui.signin.presenter.SignUpPresenter
import dagger.Module
import dagger.Provides

/**
 * Created by kavi on 21/02/18.
 */


@Module
class SignUpModule {

    @Provides
    @ActivityScope
    internal fun providesSignUpPresenter(api: EndPoints): SignUpPresenter {
        return SignUpPresenter(api)
    }
}
