package com.gograbit.gograbit.ui.signup

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import com.gograbit.gograbit.R
import com.gograbit.gograbit.base.BaseActivity
import com.gograbit.gograbit.ui.signin.presenter.SignUpPresenter
import com.gograbit.gograbit.ui.signin.presenter.SignUpView
import com.gograbit.gograbit.ui.signup.di.DaggerSignUpComponent
import com.gograbit.gograbit.ui.signup.di.SignUpModule
import kotlinx.android.synthetic.main.activity_sign_up.*
import javax.inject.Inject

class SignUpActivity : BaseActivity(), SignUpView {
    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar.visibility = View.GONE
    }

    @Inject
    lateinit var presenter: SignUpPresenter

    override fun navigateToListActivity() {
        Toast.makeText(this, "Success", Toast.LENGTH_LONG).show()
        finish()
    }

    override fun setEmailEmptyError() {

        emailEdt.setError(getString(R.string.email_empty))
    }

    override fun setEmailInvalidError() {
        emailEdt.setError(getString(R.string.email_invalid))
    }

    override fun setPasswordEmptyError() {
        passwordEdt.setError(getString(R.string.password_empty))
    }

    override fun setPasswordInvalidError() {
        passwordEdt.setError(getString(R.string.password_invalid))
    }

    override fun setFirstnameEmptyError() {
        fNameEdt.setError(getString(R.string.first_name_empty))
    }

    override fun setLastnameEmptyError() {
        lNameEdt.setError(getString(R.string.last_name_empty))
    }

    override fun onActivityInject() {
        DaggerSignUpComponent.builder().appComponent(getAppcomponent())
                .signUpModule(SignUpModule())
                .build()
                .inject(this);

        presenter.attachView(this)


    }

    private fun triggerSignUpRequest() {

        if (presenter.validateFields(emailEdt.text.toString(), passwordEdt.text.toString(), fNameEdt.text.toString(), lNameEdt.text.toString()))
            presenter.registerUser(fNameEdt.text.toString(), lNameEdt.text.toString(), emailEdt.text.toString(), passwordEdt.text.toString())

    }


    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.sign_up)
        setContentView(R.layout.activity_sign_up)

        signUpBtn.setOnClickListener(View.OnClickListener { triggerSignUpRequest() })


        passwordEdt.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    triggerSignUpRequest()
                    return true
                }
                return false
            }
        })

    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.getItemId()) {
            android.R.id.home -> {
                // ProjectsActivity is my 'home' activity
                finish();
                return true
            }
        }
        return super.onOptionsItemSelected(menuItem)
    }

}
