package com.gograbit.gograbit.ui.signin.presenter

import com.gograbit.gograbit.base.di.mvp.BaseView

/**
 * Created by kavi on 21/02/18.
 */

interface SignInView : BaseView {
    fun navigateToListActivity()

    fun setEmailEmptyError()

    fun setEmailInvalidError()

    fun setPasswordEmptyError()

    fun setPasswordInvalidError()


}
