package com.gograbit.gograbit.ui.signup.di

import com.gograbit.gograbit.base.di.ActivityScope
import com.gograbit.gograbit.base.di.component.AppComponent
import com.gograbit.gograbit.ui.signup.SignUpActivity
import dagger.Component

/**
 * Created by kavi on 21/02/18.
 */


@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(SignUpModule::class))
interface SignUpComponent {

    fun inject(signUpActivity: SignUpActivity)
}
