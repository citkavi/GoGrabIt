package com.gograbit.gograbit.ui.signin.model
import com.google.gson.annotations.SerializedName


/**
 * Created by kavi on 21/02/18.
 */

data class SignInRequest(
		@SerializedName("username") var username: String = "",
		@SerializedName("password") var password: String = ""
)