package com.gograbit.gograbit.ui.signin.di

import com.gograbit.gograbit.base.api.EndPoints
import com.gograbit.gograbit.base.di.ActivityScope
import com.gograbit.gograbit.ui.signin.presenter.SignInPresenter
import dagger.Module
import dagger.Provides

/**
 * Created by kavi on 21/02/18.
 */


@Module
class SignInModule {

    @Provides
    @ActivityScope
    internal fun providesHomePresenter(api: EndPoints): SignInPresenter {
        return SignInPresenter(api)
    }
}
