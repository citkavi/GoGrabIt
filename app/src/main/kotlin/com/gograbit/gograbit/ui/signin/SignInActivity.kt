package com.gograbit.gograbit.ui.signin

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import com.gograbit.gograbit.R
import com.gograbit.gograbit.base.BaseActivity
import com.gograbit.gograbit.ui.signin.di.DaggerSignInComponent
import com.gograbit.gograbit.ui.signin.di.SignInModule
import com.gograbit.gograbit.ui.signin.presenter.SignInPresenter
import com.gograbit.gograbit.ui.signin.presenter.SignInView
import com.gograbit.gograbit.ui.signup.SignUpActivity
import kotlinx.android.synthetic.main.activity_sign_in.*
import javax.inject.Inject


class SignInActivity : BaseActivity(), SignInView {

    @Inject
    lateinit var presenter: SignInPresenter

    override fun navigateToListActivity() {

        Toast.makeText(this, "Success", Toast.LENGTH_LONG).show()
    }

    override fun onActivityInject() {

        DaggerSignInComponent.builder().appComponent(getAppcomponent())
                .signInModule(SignInModule())
                .build()
                .inject(this);

        presenter.attachView(this)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        setUpListeners()
    }

    private fun setUpListeners() {
        signInBtn.setOnClickListener(View.OnClickListener { triggerSignInApi() })

            signUpLayout.setOnClickListener(View.OnClickListener {
                val intent = Intent(this, SignUpActivity::class.java)
                startActivity(intent)
            })


        passwordEdt.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    triggerSignInApi()
                    return true
                }
                return false
            }
        })
    }


    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar.visibility = View.GONE
    }


    private fun triggerSignInApi() {
        if (presenter.isValidEmail(emailEdt.text.toString()) && presenter.isPasswordNotEmpty(passwordEdt.text.toString()))
            presenter.loginUser(emailEdt.text.toString(), passwordEdt.text.toString())
    }

    override fun setEmailEmptyError() {
        emailEdt.setError(getString(R.string.email_empty))
    }

    override fun setEmailInvalidError() {
        emailEdt.setError(getString(R.string.email_invalid))
    }

    override fun setPasswordEmptyError() {
        passwordEdt.setError(getString(R.string.password_empty))
    }

    override fun setPasswordInvalidError() {
        passwordEdt.setError(getString(R.string.password_invalid))
    }


}
