package com.gograbit.gograbit.ui.signin.presenter

import com.gograbit.gograbit.base.api.EndPoints
import com.gograbit.gograbit.base.di.mvp.BasePresenter
import com.gograbit.gograbit.ui.signin.model.SignInErrorResponse
import com.gograbit.gograbit.ui.signin.model.SignInRequest
import com.gograbit.gograbit.ui.signin.model.SignInResponse
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.regex.Pattern
import javax.inject.Inject

/**
 * Created by kavi on 21/02/18.
 */


class SignInPresenter @Inject constructor(var api: EndPoints) : BasePresenter<SignInView>() {


    fun loginUser(userName: String, password: String) {
        view?.showProgress()

        api.signInUser(SignInRequest(userName, password)).enqueue(object : Callback<SignInResponse> {
            override fun onResponse(call: Call<SignInResponse>, response: Response<SignInResponse>) {

                view?.hideProgress()

                if (response.isSuccessful) {
                    view?.navigateToListActivity()
                } else {
                    val gson = Gson()
                    var error: SignInErrorResponse = gson.fromJson(response.errorBody().string(), SignInErrorResponse::class.java)
                    view?.onError(error.message)
                }

            }

            override fun onFailure(call: Call<SignInResponse>, t: Throwable) {
                view?.onError()
                view?.hideProgress()
            }
        })
    }

    fun isValidEmail(emailId: String): Boolean {
        if (isEmailNotEmpty(emailId) && isEmailValid(emailId))
            return true

        return false
    }

    fun isEmailNotEmpty(emailId: String): Boolean {
        if (emailId.length == 0) {
            view?.setEmailEmptyError()
            return false
        }
        return true
    }

    fun isEmailValid(emailId: String): Boolean {
        if (!Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(emailId).matches()) {
            view?.setEmailInvalidError()
            return false
        }
        return true
    }

    fun isPasswordNotEmpty(password: String): Boolean {
        if (password.length == 0) {
            view?.setPasswordEmptyError()
            return false
        }
        return true
    }

}
