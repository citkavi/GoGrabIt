package com.gograbit.gograbit.ui.signup.model

import com.google.gson.annotations.SerializedName


/**
 * Created by kavi on 21/02/18.
 */

data class SignUpResponse(
        @SerializedName("status") var status: Boolean = false,
        @SerializedName("member_id") var memberId: Int = 0,
        @SerializedName("message") var message: String = ""
)