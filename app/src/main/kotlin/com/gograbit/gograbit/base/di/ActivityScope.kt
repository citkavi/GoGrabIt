package com.gograbit.gograbit.base.di

/**
 * Created by kavi on 21/02/18.
 */

import javax.inject.Scope

@Scope @Retention annotation class ActivityScope
