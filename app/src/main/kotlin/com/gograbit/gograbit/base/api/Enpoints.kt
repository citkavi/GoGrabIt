package com.gograbit.gograbit.base.api

import com.gograbit.gograbit.ui.signin.model.SignInRequest
import com.gograbit.gograbit.ui.signin.model.SignInResponse
import com.gograbit.gograbit.ui.signup.model.SignUpRequest
import com.gograbit.gograbit.ui.signup.model.SignUpResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by kavi on 21/02/18.
 */

interface EndPoints {

    @POST("account/login")
    fun signInUser(@Body signInRequest: SignInRequest): Call<SignInResponse>


    @POST("member/add")
    fun signUpUSer(@Body signUpRequest: SignUpRequest): Call<SignUpResponse>


}