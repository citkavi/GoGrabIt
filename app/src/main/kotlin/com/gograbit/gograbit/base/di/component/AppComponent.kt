package com.gograbit.gograbit.base.di.component

import android.app.Application
import android.content.res.Resources
import com.gograbit.gograbit.base.api.EndPoints
import com.gograbit.gograbit.base.di.module.ApiModule
import com.gograbit.gograbit.base.di.module.AppModule
import com.gograbit.gograbit.base.di.module.OkHttpModule
import com.gograbit.gograbit.base.di.module.RetrofitModule
import com.gograbit.gograbit.base.helper.SpHelper
import com.google.gson.Gson
import dagger.Component
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by kavi on 21/02/18.
 */


@Singleton
@Component(modules = arrayOf(AppModule::class, RetrofitModule::class, ApiModule::class, OkHttpModule::class))
interface AppComponent {
    fun application(): Application
    fun gson(): Gson
    fun resources(): Resources
    fun retrofit(): Retrofit
    fun endpoints(): EndPoints
    fun cache(): Cache
    fun client(): OkHttpClient
    fun loggingInterceptor(): HttpLoggingInterceptor
    fun spHelper(): SpHelper
}