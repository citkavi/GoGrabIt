package com.gograbit.gograbit.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.gograbit.gograbit.base.di.component.AppComponent
import com.gograbit.gograbit.base.di.mvp.BasePresenter
import com.gograbit.gograbit.base.di.mvp.BaseView
import com.gograbit.gograbit.base.helper.GoGrabItApplication
import org.jetbrains.anko.toast

/**
 * Created by kavi on 21/02/18.
 */

abstract class BaseActivity : AppCompatActivity(), BaseView {

    private var presenter: BasePresenter<*>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onActivityInject()
    }

    protected abstract fun onActivityInject()

    fun getAppcomponent(): AppComponent = GoGrabItApplication.appComponent

    override fun setPresenter(presenter: BasePresenter<*>) {
        this.presenter = presenter
    }

    override fun onError() {
        toast("Something went wrong")
    }

    override fun onError(message: String) {
        toast(message)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }



    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
        presenter = null
    }


}
