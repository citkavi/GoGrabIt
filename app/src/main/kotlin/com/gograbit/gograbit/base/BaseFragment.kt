package com.gograbit.gograbit.base

import android.os.Bundle
import android.support.v4.app.Fragment
import com.gograbit.gograbit.base.di.component.AppComponent
import com.gograbit.gograbit.base.di.mvp.BasePresenter
import com.gograbit.gograbit.base.di.mvp.BaseView
import com.gograbit.gograbit.base.helper.GoGrabItApplication

/**
 * Created by kavi on 21/02/18.
 */

abstract class BaseFragment : Fragment(), BaseView {

    private var presenter: BasePresenter<*>? = null

    protected abstract fun onActivityInject()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onActivityInject()
    }


    fun getAppcomponent(): AppComponent = GoGrabItApplication.appComponent

    override fun setPresenter(presenter: BasePresenter<*>) {
        this.presenter = presenter
    }


    override fun onError() {
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }



    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
        presenter = null
    }
}