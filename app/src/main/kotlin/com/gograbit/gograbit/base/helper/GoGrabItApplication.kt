package com.gograbit.gograbit.base.helper

import android.app.Application
import com.gograbit.gograbit.base.di.component.AppComponent
import com.gograbit.gograbit.base.di.component.DaggerAppComponent
import com.gograbit.gograbit.base.di.module.AppModule

/**
 * Created by kavi on 21/02/18.
 */



class GoGrabItApplication: Application() {

    companion object{
        @JvmStatic lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDagger()
    }

    private fun initDagger() {
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }

}