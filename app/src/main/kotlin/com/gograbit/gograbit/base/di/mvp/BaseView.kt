package com.gograbit.gograbit.base.di.mvp

/**
 * Created by kavi on 21/02/18.
 */


interface BaseView {
    fun onError()
    fun onError(message:String)

    fun setPresenter(presenter: BasePresenter<*>)

    fun showProgress()

    fun hideProgress()
}
