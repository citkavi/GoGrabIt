package com.gograbit.gograbit.base.di.mvp

/**
 * Created by kavi on 21/02/18.
 */


interface Presenter<V : BaseView> {

    fun attachView(view: V)

    fun detachView()
}
