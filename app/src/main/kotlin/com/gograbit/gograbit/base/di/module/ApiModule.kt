package com.gograbit.gograbit.base.di.module

import com.gograbit.gograbit.base.api.EndPoints
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by kavi on 21/02/18.
 */

@Module
class ApiModule {
    @Provides
    @Singleton
    fun providesEndpoints(retrofit: Retrofit): EndPoints = retrofit.create(EndPoints::class.java)
}