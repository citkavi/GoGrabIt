package com.gograbit.gograbit.ui.signup.presenter

import com.gograbit.gograbit.base.api.EndPoints
import com.gograbit.gograbit.ui.signin.presenter.SignUpPresenter
import com.gograbit.gograbit.ui.signin.presenter.SignUpView
import com.gograbit.gograbit.ui.signup.model.SignUpRequest
import com.gograbit.gograbit.ui.signup.model.SignUpResponse
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doAnswer
import com.nhaarman.mockito_kotlin.mock
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by kavi on 22/02/18.
 */


class SignUpPresenterTest {


    private val view: SignUpView = mock()
    private val api: EndPoints = mock()
    private lateinit var presenter: SignUpPresenter


    @Before
    fun setup() {
        presenter = SignUpPresenter(api)
        presenter.attachView(view)

    }


    @Test
    fun test_login_user_success() {

        val mockedCall: Call<SignUpResponse> = mock()

        val mockedResponse: SignUpResponse = mock()

        Mockito.`when`(api.signUpUSer(SignUpRequest("abc", "def", "citkavi@gmail.com", "password", 1, "password"))).thenReturn(mockedCall)

        doAnswer {
            val callBack: Callback<SignUpResponse> = it.getArgument(0)

            callBack.onResponse(mockedCall, Response.success(mockedResponse))
        }.`when`(mockedCall).enqueue(any())


        presenter.registerUser("abc", "def", "citkavi@gmail.com", "password")

        Mockito.verify(view).navigateToListActivity()

    }

    @Test
    fun test_login_user_error() {
        val mockThrowable: Throwable = mock()

        val mockedCall: Call<SignUpResponse> = mock()

        Mockito.`when`(api.signUpUSer(SignUpRequest("abc", "def", "citkavi@gmail.com", "password", 1, "password"))).thenReturn(mockedCall)

        doAnswer {
            val callBack: Callback<SignUpResponse> = it.getArgument(0)

            callBack.onFailure(mockedCall, mockThrowable)
        }.`when`(mockedCall).enqueue(any())

        presenter.registerUser("abc", "def", "citkavi@gmail.com", "password")

        Mockito.verify(view).onError()

    }


    @Test
    fun test_email_empty_success() {
        Assert.assertEquals(presenter.isEmailNotEmpty(""), true)
        Mockito.verify(view).setEmailEmptyError()
    }

    @Test
    fun test_email_empty_fail() {
        Assert.assertEquals(presenter.isEmailNotEmpty("abcd@def.com"), false)

    }

    @Test
    fun test_email_invalid_success() {
        Assert.assertEquals(presenter.isEmailValid("abcd"), true)
        Mockito.verify(view).setEmailInvalidError()

    }

    @Test
    fun test_email_invalid_fail() {
        Assert.assertEquals(presenter.isEmailValid("abcd@gmail.com"), false)
        Mockito.verify(view).setEmailInvalidError()

    }

    @Test
    fun test_password_empty_fail() {
        Assert.assertEquals(presenter.isPasswordNotEmpty(""), false)
        Mockito.verify(view).setEmailInvalidError()

    }

    @Test
    fun test_password_empty_success() {
        Assert.assertEquals(presenter.isPasswordNotEmpty("abcdef"), true)
        Mockito.verify(view).setEmailInvalidError()
    }

}