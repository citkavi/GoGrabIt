package com.gograbit.gograbit.ui.signin.presenter

import com.gograbit.gograbit.base.api.EndPoints
import com.gograbit.gograbit.ui.signin.model.SignInRequest
import com.gograbit.gograbit.ui.signin.model.SignInResponse
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doAnswer
import com.nhaarman.mockito_kotlin.mock
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by kavi on 22/02/18.
 */


class SignInPresenterTest {

    private val view: SignInView = mock()
    private val api: EndPoints = mock()
    private lateinit var presenter: SignInPresenter


    @Before
    fun setup() {
        presenter = SignInPresenter(api)
        presenter.attachView(view)

    }


    @Test
    fun test_login_user_success() {

        val mockedCall: Call<SignInResponse> = mock()

        val mockedResponse: SignInResponse = mock()

        Mockito.`when`(api.signInUser(SignInRequest("citkavi@gmail.com", "password"))).thenReturn(mockedCall)

        doAnswer {
            val callBack: Callback<SignInResponse> = it.getArgument(0)

            callBack.onResponse(mockedCall, Response.success(mockedResponse))
        }.`when`(mockedCall).enqueue(any())


        presenter.loginUser("citkavi@gmail.com", "password")

        Mockito.verify(view).navigateToListActivity()

    }

    @Test
    fun test_login_user_error() {
        val mockThrowable: Throwable = mock()

        val mockedCall: Call<SignInResponse> = mock()

        Mockito.`when`(api.signInUser(SignInRequest("citkavi@gmail", "password"))).thenReturn(mockedCall)

        doAnswer {
            val callBack: Callback<SignInResponse> = it.getArgument(0)

            callBack.onFailure(mockedCall, mockThrowable)
        }.`when`(mockedCall).enqueue(any())

        presenter.loginUser("citkavi@gmail", "password")

        Mockito.verify(view).onError()

    }


    @Test
    fun test_email_empty_success() {
        Assert.assertEquals(presenter.isEmailNotEmpty(""), true)
        Mockito.verify(view).setEmailEmptyError()

    }


    @Test
    fun test_email_empty_fail() {
        Assert.assertEquals(presenter.isEmailNotEmpty("abcd@def.com"), false)

    }

    @Test
    fun test_email_invalid_success() {
        Assert.assertEquals(presenter.isEmailValid("abcd"), true)
        Mockito.verify(view).setEmailInvalidError()

    }

    @Test
    fun test_email_invalid_fail() {
        Assert.assertEquals(presenter.isEmailValid("abcd@gmail.com"), false)
        Mockito.verify(view).setEmailInvalidError()

    }
}