package com.tala.base.mvp

import com.gograbit.gograbit.base.di.mvp.BasePresenter
import com.gograbit.gograbit.base.di.mvp.BaseView
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test

/**
 * Created by kavi on 21/02/18.
 */
class BasePresenterTest {

    private lateinit var basePresenter: BasePresenter<BaseView>
    private val view: BaseView = mock()

    @Before
    fun setUp() {
        basePresenter = BasePresenter()
    }

    @Test
    fun attachView() {
        basePresenter.attachView(view)

        verify(view).setPresenter(basePresenter)
    }

}